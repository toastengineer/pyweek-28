import functools
import inspect
import sys
from pathlib import Path

import character as characters

import musicplayer
from text_menu import PopUpMenu, pop_up_prompt, BreakOutOfMenu, pop_up_notification, pop_up_yes_no

quit_confirmation = PopUpMenu(
    Whoops_nevermind = None,
    Really_quit = lambda _: sys.exit(),
    prompt = "Are you sure you want to quit?"
)

_class_select = PopUpMenu({c.class_name: c for c in characters.PlayerCharacter.__subclasses__()})

def add_party_member(gamestate):
    name = pop_up_prompt("Name: ")
    thepc = _class_select(name)
    gamestate.party.append(thepc)
    import gfx
    gfx.main_window.bottom_textbox.print_character_blocks(gamestate.party)

def remove_party_member(gamestate):
    PopUpMenu({
        str(p): functools.partial(gamestate.party.remove, p)
        for p in gamestate.party
    })()
    import gfx
    gfx.main_window.bottom_textbox.print_character_blocks(gamestate.party)

def manage_party(gamestate):
    PopUpMenu({
        "Create new character": add_party_member,
        "Remove character"    : remove_party_member,
        "Never mind"          : None
    }, loop_until_non_callable = True)(gamestate)

def item_shop(gamestate):
    while True:
        print(f"You have ${gamestate.partyfunds}")

        itempicker = PopUpMenu({f"{str(cls())} : ${cls.sale_price}" : cls for cls in characters.shop_items})
        itempicker["Never mind"] = None

        item = itempicker()
        if item is None: return

        info = inspect.getdoc(item)
        if info:
            print(info)

        if not gamestate.party:
            print("You can look at items all you want, but you can't actually buy anything without party members to hold it.")
            continue

        characters_with_room = [c for c in gamestate.party if len(c.inventory) < 6]

        if not characters_with_room:
            print("All your characters have full packs. Try going to \"manage party inventory\" and getting rid of some stuff.")
            continue

        if gamestate.partyfunds < item.sale_price:
            print("You don't have enough money.")
            continue

        select_recipient = PopUpMenu(characters_with_room)
        select_recipient["Never mind"] = None

        recipient = select_recipient()

        if recipient is None:
            continue

        recipient.inventory.append(item)
        print(f"Bought a(n) {item} for {recipient} for ${item.sale_price}")
        gamestate.partyfunds -= item.sale_price

class TownMenu(PopUpMenu):

    def __call__(self, gamestate):
        #music = musicplayer.ChunkedMusicPlayer(Path("music", "preparation"), lambda: len(gamestate.party))
        #music.spawn_thread()
        if gamestate.party:
            for thecharacter in gamestate.party:
                thecharacter.physical_stamina = thecharacter.max_physical_stamina
                thecharacter.mental_stamina = thecharacter.max_mental_stamina
            print("All characters restored to maximum stamina.")
        super().__call__(gamestate)
        #music.stop(fadeout=1)

def enter_tower(gamestate):
    testman = characters.Warrior("Testman")
    testman.inventory.append(characters.PointyStick())
    testman.inventory.append(characters.Carbine())
    #testman.inventory.append(characters.CombatFirstAidKit())
    gamestate.party.append(testman)
    #if len(gamestate.party) == 0:
    #    pop_up_notification("You need to assemble a party before you can begin the adventure.")
    #    return
    #if len(gamestate.party) < 6:
    #    if not pop_up_yes_no("Are you sure you want to enter the tower with less than a full team of 6?"):
    #        return
    import world
    gamestate.current_level = world.World()
    raise BreakOutOfMenu

town_menu = TownMenu({
    "The Tower's Shadow Inn" : manage_party,
    "Adventurer's Supply Store" : item_shop,
    #"Alliterative Arnold's Affordable Adventuring Accessories": (lambda x: None),
    #"A Terrible-Smelling Back Alley" : (lambda x: None),
    #"Public Workshop" : (lambda x: None),
    #"Manage party inventory": (lambda x: None),
    "Enter the Tower" : enter_tower,
    "Quit the Game"   : quit_confirmation
}, loop_until_non_callable = True,)
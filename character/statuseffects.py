class StatusEffect:
    incapacitating = False
    abbreviation = ''

    def __init__(self, turns_left):
        self.turns_left = turns_left

    @classmethod
    def apply(cls, character, *args, **kwargs):
        """Initializes the StatusEffect as normal and immediately applies it to a character."""
        if not any(isinstance(theeffect, cls) for theeffect in character.status_effects):
            character.status_effects.append(cls(*args, **kwargs))

    def each_turn(self, character):
        """Return True to have time_up called and get removed from the status effects list"""
        self.turns_left -= 1
        return self.turns_left <= 0

    def time_up(self, character):
        pass


class BleedingOut(StatusEffect):
    abbreviation = "BLEED"
    def __init__(self):
        super().__init__(25)

    def each_turn(self, character):
        print(f"{character} is bleeding to death!")

    def time_up(self, character):
        character.kill()
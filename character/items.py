import re
import typing

import minigames
from . import statuseffects

if typing.TYPE_CHECKING:
    from character import *
    from battle import BattleState

class Affordance:
    """Represents a thing an item can be used to do.

    Verb is the verb this affordance represents, i.e. "drink," "attack," "set timer," etc...

    Methodname is the name of the method on the object that implements the affordance.

    After that, provide either strings or lists of items; these tell callers what parameters
    the method expects. If a list is provided, this indicates the caller must pick out of the list.
    The strings represent certain lists or objects, i.e. "friend", "enemy" mean the method should
    be passed a character friendly to the user, an enemy character.
    """

    def __init__(self, verb, method, *args, can_use_in_battle = False,):
        self.verb = verb
        self.method = method
        self.params = args
        self.can_use_in_battle = can_use_in_battle

    @property
    def can_use_out_of_battle(self):
        return "battle" not in self.params and "enemy" not in self.params


class Item:
    sale_price = 100
    minigame = "typing_tutor"
    skill = "tech"
    harmful = False

    @property
    def name(self):
        s1 = re.sub('(.)([A-Z][a-z]+)', r'\1 \2', type(self).__name__)
        name = re.sub('([a-z0-9])([A-Z])', r'\1 \2', s1).lower()
        return name

    def __str__(self):
        return self.name

    def skill_check_minigame(self, character: 'Character', target: 'Character'):
        difficulty = getattr(target, self.skill) / getattr(character, self.skill)

        import battle
        if battle.ESTIMATING_FUTURE:
            return max(0, min(1, difficulty))

        minigame = getattr(minigames, self.minigame)

        if character.is_pc:
            effectiveness = minigame(difficulty)
            setattr(character, self.skill, getattr(character, self.skill) + (effectiveness / 4))
        else:
           effectiveness = minigame(1 / difficulty)

        return effectiveness

    @property
    def affordances(self):
        return []

class MeleeWeapon(Item):
    """Represents a close range weapon that affords attacking an enemy in battle."""
    minigame = "test_your_strength"
    power = 1
    affordances = [Affordance("attack", "attack", "user", "enemy")]

    def attack(self, user: 'Character', enemy: 'Character'):
        effectiveness = self.skill_check_minigame(user, enemy)
        user.physical_stamina  -= self.power / 1 - effectiveness
        enemy.physical_stamina -= self.power * effectiveness

class PointyStick(MeleeWeapon):
    power = 2

class Axe(MeleeWeapon):
    power = 4


class RangedWeapon(Item):
    """Represents a ranged weapon that affords attacking an enemy in battle."""
    minigame = "drunk_darts"
    power = 1
    affordances = [Affordance("shoot", "attack", "user", "enemy", "battle")]

    def attack(self, user: 'Character', enemy: 'Character', battle: 'BattleState'):
        effectiveness = self.skill_check_minigame(user, enemy)
        user.physical_stamina  -= self.power / 1 - effectiveness
        enemy.physical_stamina -= self.power * effectiveness

class Pistol(RangedWeapon):
    power = 2

class Carbine(RangedWeapon):
    power = 5

shop_items = [PointyStick, Axe, Pistol, Carbine]
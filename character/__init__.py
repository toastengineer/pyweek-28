import collections
import copy
import time

from .items import *
from .statuseffects import *

if typing.TYPE_CHECKING:
    import battle

class LimitedList(list):

    def __init__(self, iterable=(), limit=0):
        super().__init__(iterable)
        self.limit = limit

    def append(self, object):
        if len(self) >= self.limit:
            raise ValueError(f"Can't add {object}, only {self.limit} items allowed")
        super(LimitedList, self).append(object)


class BattleAction:
    def __call__(self, battle: 'battle.Battle'):
        pass

class UseItem(BattleAction):
    def __init__(self, item, user, target):
        self.item = item
        self.user = user
        self.target = target

    def __str__(self):
        if self.item.harmful:
            return f"attack {self.target} with {self.item}"
        else:
            return f"use {self.item} on {self.target}"
    def __repr__(self):
        return f"<{str(self)}>"

    def __call__(self, battle):
        should_remain = self.item.use_in_combat(self.user, self.target)
        if not should_remain:
            try:
                self.user.inventory.remove(self.item)
            except ValueError:
                pass

class UseAffordance(BattleAction):
    def __init__(self, user, item, affordance, target):
        self.user       = user
        self.item       = item
        self.affordance = affordance
        self.target     = target
        self.is_attack  = "enemy" in affordance.params

    def __str__(self):
        return f"{self.affordance.verb} {str(self.target) + ' with' if self.target else ''} {self.item}"
    def __repr__(self):
        return f"<{str(self)}>"

    def __call__(self, **kwargs):
        kwargs["user"] = self.user
        #This feels really dumb... but uhh... I guess it makes sense?
        kwargs["enemy"] = self.target
        kwargs["friend"] = self.target
        getattr(self.item, self.affordance.method)(*(kwargs[p] for p in self.affordance.params))


class RunAway(BattleAction):
    def __init__(self, runner):
        self.runner = runner

    def __str__(self):
        return "run away"

    def __call__(self, battle):
        if self.runner in battle.player_characters:
            battle.player_characters.remove(self.runner)
            print(f"{self.runner} runs away!")
        elif self.runner in battle.enemies:
            battle.enemies.remove(self.runner)
            print(f"{self.runner} runs away!")

class StartActionFindingOver(BattleAction):
    def __str__(self):
        return """Go back, I changed my mind!"""


class Character:

    class_name = "Generic"
    strength_gain_factor = 1
    agility_gain_factor = 1
    magic_gain_factor = 1
    tech_gain_factor = 1

    is_pc = False

    def __init__(self, name = "noname", strength = 10, agility = 10, magic = 10, tech = 10, max_physical_stamina = 20, max_mental_stamina = 20):
        self.name = name

        self.strength = strength
        self.agility = agility
        self.magic = magic
        self.tech = tech

        self.max_physical_stamina = max_physical_stamina
        self.max_mental_stamina = max_mental_stamina
        self.physical_stamina = max_physical_stamina
        self.mental_stamina = max_mental_stamina

        self.inventory = LimitedList(limit=6) #type: list[Item]

        self.status_effects = [] #type: list[StatusEffect]

        self.dead = False

        #For identifying an individual character accross deepcopies
        self.id = id(self)

    def __eq__(self, other):
        return isinstance(other, Character) and self.id == other.id

    def __hash__(self):
        return self.id #@TODO: Bad idea?

    def __repr__(self):
        return f"{self.class_name}({self.name}, {self.strength}, {self.agility}, {self.magic}, {self.tech}, {self.max_physical_stamina}, {self.max_mental_stamina})"

    def __str__(self):
        return self.name

    def __format__(self, format_spec):
        return format(self.name, format_spec)

    def stat_block(self, inventory = False):
        n = "\n"
        return (
f"""{self.name} {"DEAD" if self.dead else "INCAPACITATED" if self.incapacitated else ""}
{self.class_name}

ST: {self.strength:.1f} AG: {self.agility:.1f}
MG: {self.magic:.1f} TH: {self.tech:.1f}
Phys Stam: {self.physical_stamina:.1f} / {self.max_physical_stamina:.1f}
Mntl Stam: {self.mental_stamina:.1f} / {self.max_mental_stamina:.1f}
{' '.join(eff.abbreviation for eff in self.status_effects)}{n if inventory else ""}{n.join(str(i) for i in self.inventory) if inventory else ""}
"""            )

    def process_status_effects(self):
        for idx, the_effect in enumerate(reversed(self.status_effects)):
            if the_effect.each_turn(self):
                del self.status_effects[idx]
                the_effect.time_up(self)

    def kill(self):
        self.dead = True

    @property
    def incapacitated(self):
        if self.dead: return True
        if self.physical_stamina <= 0: return True
        if self.mental_stamina <= 0: return True
        for the_effect in self.status_effects:
            if the_effect.incapacitating:
                return True
        return False
    @incapacitated.setter
    def incapacitated(self, x):
        if not x:
            self.status_effects[:] = [e for e in self.status_effects
                                      if not e.incapacitating]
            if self.physical_stamina <= 0: self.physical_stamina = self.max_physical_stamina / 10
            if self.mental_stamina <= 0: self.mental_stamina = self.max_mental_stamina / 10
        else:
            #Dunno why you'd ever want to do this...
            self.physical_stamina = 0

    def collect_battle_actions(self, battle):
        """Return a list of all possible things this character can
        do in the given fight. Most of them are probably bad ideas."""
        if self.dead: return []

        actions = []

        attacks   = []
        utilities = []
        for the_item in self.inventory:
            for the_affordance in the_item.affordances:
                if "enemy" in the_affordance.params:
                    attacks.append((the_item, the_affordance))
                elif "friend" in the_affordance.params and the_affordance.can_use_in_battle:
                    utilities.append((the_item, the_affordance))

        us, them = battle.teams(self)

        for the_item, the_affordance in attacks:
            for the_enemy in them:
                actions.append(UseAffordance(self, the_item, the_affordance, the_enemy))

        for the_item, the_affordance in utilities:
            for the_friend in us:
                actions.append(UseAffordance(self, the_item, the_affordance, the_friend))

        if not actions: #npcs should only run if they have no other choice
            actions.append(RunAway(self))

        return actions

    def heuristic(self, battle: 'battle.BattleState'):
        """Return how much we like the given battle state. Combat AI's utility function."""
        try:
            my_team, enemy_team = battle.teams(self)
        except ValueError: #We're not in the battle for some reason, maybe we ran away
            return -9999

        #It should probably be weighted by which kind of stamina this character's abilities use or something...
        my_team_total_stamina    = 0
        enemy_team_total_stamina = 0

        for thecharacter in my_team:
            my_team_total_stamina += 0 if thecharacter.incapacitated else (thecharacter.physical_stamina + thecharacter.mental_stamina)

        for thecharacter in enemy_team:
            enemy_team_total_stamina += 0 if thecharacter.incapacitated else (thecharacter.physical_stamina + thecharacter.mental_stamina)

        if my_team_total_stamina    <= 0: return -9999
        if enemy_team_total_stamina <= 0: return  9999
        return my_team_total_stamina - (enemy_team_total_stamina * 2)

    def get_next_battle_action(self, battle_state: 'battle.BattleState'):
        actions = self.collect_battle_actions(battle_state)
        if not actions:
            return lambda: print(f"{self} couldn't do anything.")

        start_time = time.perf_counter()
        import battle
        with battle.imagination():
            state_queue = [((), battle_state, 1)]
            futures = collections.defaultdict(list)
            endgames = []
            while state_queue:
                action_chain, battle_state, depth = state_queue.pop(0)
                me = next((p for p in battle_state.participants if p == self), None)
                if me is None or battle_state.is_over:
                    endgames.append((action_chain, battle_state, depth))
                    continue
                for the_action in me.collect_battle_actions(battle_state):
                    descendant_state = copy.deepcopy(battle_state)
                    the_action(battle=descendant_state)
                    action_chain_state_pair = (action_chain + (the_action,), descendant_state, depth + 1)
                    futures[depth].append(action_chain_state_pair)
                    if depth < 5:
                        state_queue.append(action_chain_state_pair)
                if time.perf_counter() - start_time > 0.05:
                   del futures[max(futures.keys())] #delete the incomplete layer
                   break

        if not futures: return lambda x: print(f"{self} couldn't do anything!")
        depth = max(futures.keys())
        futures[depth] += endgames
        futures[depth].sort(key = lambda x: len(x[0]))
        #from pprint import pprint; pprint(list((best_action_chain, self.heuristic(state)) for best_action_chain, state, _ in futures[depth]), stream=sys.__stdout__)
        best_action_chain, best_state, _ = max(futures[depth], key = lambda x: self.heuristic(x[1]))
        if not best_action_chain: return lambda x: print(f"{self} couldn't do anything!") #we won at battle start somehow...
        return best_action_chain[0]

class MurderousJerk(Character):
    class_name = "Murderous Jerk"

class PlayerCharacter(Character):
    is_pc = True
    
    def get_next_battle_action(self, battle_state):
        import text_menu
        return text_menu.PopUpMenu(self.collect_battle_actions(battle_state), dontcall=True)()

class Wizard(PlayerCharacter):
    class_name = "Wizard"
    magic_gain_factor = 2.5

class Warrior(PlayerCharacter):
    class_name = "Warrior"
    magic_gain_factor = 2.5

class Rifleman(PlayerCharacter):
    class_name = "Rifleman"
    agility_gain_factor = 2.5

class Technologist(PlayerCharacter):
    class_name = "Technologist"
    tech_gain_factor = 2.5

class Monster(PlayerCharacter):
    class_name = "Monster"

class Robot(PlayerCharacter):
    class_name = "Robot"

class Party(LimitedList):

    def __init__(self):
        super().__init__(limit = 6)
        self.camera = rendering.Camera()

import math
import os
import sys

import controls
import windowmanager

import pygame

from vec2d import V

pygame.font.init()
import pygcurse

import rendering
import world

TARGET_RESOLUTION            = 1280, 720
TARGET_WORLD_VIEW_RESOLUTION = math.ceil(TARGET_RESOLUTION[0] * 0.9), math.ceil(TARGET_RESOLUTION[1] * 0.6)
WINDOW_TITLE                 = "The Tower of Torment"
FIXEDSYS                     = pygame.font.Font("FSEX300.ttf", 16)
BGCOLOR                      = (14,55,47)
FGCOLOR                      = (255, 233, 235)
SSAA                         = 1

#Hack because pygcurse thinks Fixedsys Excelsior is 8x17 instead of 8x16
pygcurse.calcfontsize = lambda x: (8, 16)

#I'd really hoped never to have to use Pygcurse again.
#Hack to make textboxes respect newlines
def spitintogroupsof(groupSize, theList):
    return theList.split("\n")
pygcurse.spitintogroupsof = spitintogroupsof

busted_interpretkeyevent = pygcurse.interpretkeyevent
def interpretkeyevent(keyEvent):
    try:
        if keyEvent.unicode and keyEvent.unicode.isalnum():
            return keyEvent.unicode
        else:
            return busted_interpretkeyevent(keyEvent)
    except AttributeError:
        return busted_interpretkeyevent(keyEvent)
pygcurse.interpretkeyevent = interpretkeyevent

class TopTextbox(windowmanager.TerminalWindow):

    def __init__(self):
        super().__init__((TARGET_WORLD_VIEW_RESOLUTION[0] // 8, 9), (0,0), border = False)
        self.pygsurf.fill(bgcolor=BGCOLOR)

    def write(self, text, *args, **kwargs):
        self.pygsurf.write(text, *args, **kwargs)
        sys.__stdout__.write(text)

    def flush(self):
        pass

class BottomTextbox(windowmanager.TerminalWindow):

    def __init__(self):
        super().__init__((TARGET_RESOLUTION[0] // 8, 9), (0,0), border=False)
        blockwidth = self.pygsurf.width // 6 - 1
        totalwidth = blockwidth * 6 + 6
        offset = (self.pygsurf.width - totalwidth) // 2
        self.textboxes = [pygcurse.PygcurseTextbox(self.pygsurf, (((blockwidth + 1) * t  + offset), 0, blockwidth, self.pygsurf.height),
                                                   bgcolor=BGCOLOR, wrap=False, border=None)
                          for t in range(6)]
        self.selection_enabled = False
        self.selected_character = None
        self.blockwidth = blockwidth

    def move_selection(self, plus_or_minus):
        from gamestate import gamestate
        if not gamestate.party: return

        try:
            idx = gamestate.party.index(self.selected_character)
        except IndexError:
            idx = 0 if plus_or_minus == 1 else -1
        else:
            idx += plus_or_minus

        idx %= len(gamestate.party)
        self.selected_character = gamestate.party[idx]

    @controls.action("Select Next Character", (pygame.K_d, pygame.K_RIGHT, "WheelUp"))
    def select_next_character(self):
        self.move_selection(1)
    @controls.action("Select Previous Character", (pygame.K_a, pygame.K_LEFT, "WheelDown"))
    def select_previous_character(self):
        self.move_selection(-1)

    def print_character_blocks(self, characters):
        self.pygsurf.fill(bgcolor=BGCOLOR)
        for thecharacter, thebox in zip(characters, self.textboxes):
            if thecharacter == self.selected_character:
                thebox.fgcolor = BGCOLOR; thebox.bgcolor = FGCOLOR
            else:
                thebox.bgcolor = BGCOLOR; thebox.fgcolor = FGCOLOR
            thebox.text = thecharacter.stat_block()
            thebox.update()

    def decorate(self):
        from gamestate import gamestate
        mouse_pos_px = V(*pygame.mouse.get_pos()) - V(self.rect.topleft)
        mouse_x = self.pygsurf.getcoordinatesatpixel(*mouse_pos_px)[0]
        if mouse_x is not None:
            mouse_cell = mouse_x // self.blockwidth
            if 0 <= mouse_cell < len(gamestate.party):
                self.selected_character = gamestate.party[mouse_cell]

        import gamestate
        self.print_character_blocks(gamestate.gamestate.party)
        super().decorate()

main_window = None
class MainWindow:

    def __init__(self, camera = None):
        global main_window
        main_window = self
        w, h = TARGET_RESOLUTION
        self.window_surface = win = pygame.display.set_mode(TARGET_RESOLUTION)
        pygame.display.set_caption(WINDOW_TITLE)
        pygame.display.set_icon(pygame.Surface((32,32)))
        self.rect = self.window_surface.get_rect()

        self.world_view_rect        = pygame.Rect((0,0), TARGET_WORLD_VIEW_RESOLUTION)
        self.world_view_rect.center = win.get_rect().center

        self.world_view_active = True

        if not SSAA:
            self.world_view_surface     = win.subsurface(self.world_view_rect)
        else:
            w, h = TARGET_WORLD_VIEW_RESOLUTION
            w *= SSAA
            h *= SSAA
            self.world_view_surface     = pygame.Surface((w, h))
        if not camera: camera = rendering.Camera()
        self.world_view = rendering.WorldRenderer(camera, None, self.world_view_surface)

        self.top_textbox                   = TopTextbox()
        self.top_textbox_rect              = self.top_textbox.rect
        self.top_textbox_rect.midtop       = self.rect.midtop

        sys.stdout = sys.stdin = self.top_textbox

        self.bottom_textbox                = BottomTextbox()
        self.bottom_textbox_rect           = self.bottom_textbox.rect
        self.bottom_textbox_rect.midbottom = self.rect.midbottom

    def draw(self, dont_cover_popups = False):
        win = self.window_surface #type: pygame.Surface

        if dont_cover_popups:
            pass #don't draw anything in the middle, else popups will flicker
        elif self.world_view_active:
            #World view doesn't need a blit because it's a subsurface - world is drawn directly to the backbuffer
            #Unless we have antialiasing on, in which case it is a surface, that needs to be scaled down
            self.world_view.draw()
            if SSAA:
                win.blit(
                    pygame.transform.smoothscale(self.world_view_surface,TARGET_WORLD_VIEW_RESOLUTION),
                    self.world_view_rect)
            else:
                self.world_view_surface.unlock()
                win.blit(self.world_view_surface, self.world_view_rect)
        #else:
        #    self.window_surface.fill(BGCOLOR, self.middle_textbox_rect)


        #self.top_textbox.update()
        #win.blit(self.top_textbox.surface, self.top_textbox_rect)

        #self.bottom_textbox.update()
        #win.blit(self.bottom_textbox.surface, self.bottom_textbox_rect)

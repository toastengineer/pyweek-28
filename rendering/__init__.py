#Based off this tutorial:
#https://permadi.com/1996/05/ray-casting-tutorial-table-of-contents/
import time

import numpy
import pygame
from math import *

import numba

from vec2d import V


def clamp_angle_for_comparison(unclamped_angle):
    halfpi = pi / 2
    unclamped_angle += halfpi
    unclamped_angle %= pi
    unclamped_angle -= halfpi
    return unclamped_angle

class Camera:
    """Viewpoint from which the world is drawn."""
    def __init__(self, location = (1.5, 1.5), rotation = 0, rot_degrees = None):
        self.location = V(location)
        self.rotation = radians(rot_degrees) if rot_degrees is not None else rotation

class WorldSprite:
    """An object that exists in the world at a certain location, represented by a 2D sprite."""
    height = 0.5
    direction = None #if this is not None, the sprite is drawn flat as if it was facing this yaw
    def __init__(self, imgpath, location):
        self.img = pygame.image.load(imgpath)
        self.imgpath = imgpath
        self.location = V(location)

    def __repr__(self):
        return f"{type(self).__name__}({self.imgpath}, {self.location})"

class WorldRenderer:
    """Draws the world from its camera's viewpoint to a buffer."""

    def __init__(self, camera, world = None, surface: pygame.Surface = None):
        self.camera = camera
        self.change_level(world)
        self.surf = surface

        self.w, self.h = surface.get_size()
        self.zbuffer = numpy.zeros(self.w, dtype=numpy.float_)
        # fov = 58.5 * aspectratio - 4 is a magic equation that gives a vaguely correct FOV 99% of the time.
        # determined by linear regression - gives 75 degrees for 4:3 and 100 for 16:9
        fov = radians(75) #radians(58.5 * (self.w / self.h) - 4)
        print(f"fov = {degrees(fov)}")
        # We get tracebacks in the rendering code if we have FOV of 180 or greater. Why would you want that anyway?
        if fov > radians(179): fov = radians(179)
        self.fov = fov

        self.radians_per_pixel = fov / self.w
        self.pixels_per_radian = self.w / fov

        self.ray_length = 10

        self.debug_view_scale = 40
        self.debug = False

    def change_level(self, world):
        if world is None:
            self.map = self.sprites = self.visibility = None
        else:
            self.map = world.map
            self.sprites = world.sprites
            self.visibility = numpy.ndarray(V(self.map.shape) * 100, numpy.bool_)

    #UNUSED! Couldn't get it to work, ended up just using brute force.
    def cast_ray(self, origin, theta):
        theta = theta % tau #enforce 0 - 2pi

        odx, ody = V.from_yaw(theta)
        step = abs(odx) if abs(odx) >= abs(ody) else abs(ody)
        dx = odx / step
        dy = ody / step
        x, y = origin

        pygame.draw.circle(self.surf, (0,255,0), round(origin * self.debug_view_scale), 3)

        pygame.draw.line(self.surf, (0,255,0),
                         round(origin * self.debug_view_scale),
                         round(V(x, y) * self.debug_view_scale))

        def drawline():
            pygame.draw.line(self.surf, (200 / (t + 1), 200 / (t + 1), 255),
                             round(V(oldx, oldy) * self.debug_view_scale),
                             round(V(x, y) * self.debug_view_scale))

        for t in range(self.ray_length * 10):
            oldx, oldy = x, y

            x += dx

            hit = world.is_opaque(V(x, y))
            if hit:
                if x % 1 > 0.01:
                    x, y = oldx, oldy
                    dx /= 2
                    dy /= 2
                    continue
                else:
                    drawline()
                    return V(x, y)

            y += dy
            hit = world.is_opaque(V(x, y))
            if hit:
                if y % 1 > 0.01:
                    x, y = oldx, oldy
                    dx /= 2
                    dy /= 2
                else:
                    drawline()
                    return V(x, y)

            drawline()


    def draw(self):
        if self.map is None: raise ValueError("no map")
        self.surf.lock()
        self.surf.fill((0,0,0))
        debug = self.debug

        #Draw the world top-down for debugging...
        if debug:
            for loc, opaque in ((V(x, y), self.map[x, y]) for x in range(self.map.shape[0]) for y in range(self.map.shape[1])):
                rect = pygame.Rect(tuple(loc * self.debug_view_scale), (self.debug_view_scale,self.debug_view_scale))
                pygame.draw.rect(self.surf, (127,127,127), rect, 0 if opaque else 1)

        self.visibility[:] = False

        camera_location = self.camera.location
        angular_resolution = self.radians_per_pixel
        ray_length = self.ray_length

        theta = self.camera.rotation - (self.fov / 2)
        for x in range(self.w):
            hitx, hity, out_of_bounds = self.cast_ray_naive(self.map, self.visibility, *camera_location, ray_length, *V.from_yaw(theta))
            hitpos = V(hitx, hity)

            #draw the rays if we're debugging
            if debug:
                pygame.draw.line(self.surf, (64,64,64) if not out_of_bounds else (128, 64, 64),
                                 camera_location * self.debug_view_scale,
                                 hitpos * self.debug_view_scale)

            if out_of_bounds: dist = inf
            else:       dist = camera_location.distance(hitpos)

            dist *= (cos(self.camera.rotation - theta) * 1)
            self.zbuffer[x] = dist
            #dist *= (1 - ((1 + sin(theta * 5 + (time.monotonic() / 5) )) / 10)) #'walls breathing' effect
            if dist == 0: continue
            idist = 1 - (dist / self.ray_length)
            if isfinite(idist):
                l = 1 / dist * 600
                mid = self.h//2
                shade = int(255 * idist)
                if not 0 <= shade <= 255:
                    shade = min(max(shade, 0), 255)
                shade_rounded = int(shade ** 2 * 0.00385)
                if not debug:
                    try:
                        pygame.draw.line(self.surf, (shade_rounded, shade_rounded, shade_rounded),
                                         (x, mid - l/2), (x, mid + l//2))
                    except AttributeError:
                        print((x, mid - l/2), (x, mid + l//2))

            theta += angular_resolution

        # if self.debug:
        #     for loc in numpy.transpose(self.visibility.nonzero()):
        #         tenthscale = 2 #??????
        #         loc = V(loc) * tenthscale
        #         rect = pygame.Rect(tuple(loc * tenthscale), (tenthscale, tenthscale))
        #         pygame.draw.rect(self.surf, (0, 200, 0, 50), rect)

        self.surf.unlock()

        self.draw_sprites()
        #quit()

    @staticmethod
    @numba.jit(nopython=True)
    def cast_ray_naive(world, visibility, check_x, check_y, ray_length, ray_x, ray_y):
        ray_x /= 125
        ray_y /= 125
        maxx, maxy = world.shape
        for t in range(ray_length * 125):
            check_x += ray_x
            if not 0 <= floor(check_x) < maxx:
                return floor(check_x), check_y, True

            if world[floor(check_x), floor(check_y)]:
                # It'll be some ways in to the blocked of square -
                # this moves the point on the grid line, to where it actually
                # intersected the wall
                check_x = round(check_x)
                break

            check_y += ray_y
            if not 0 <= floor(check_y) < maxy:
                return floor(check_y), check_y, True

            if world[floor(check_x), floor(check_y)]:
                check_y = round(check_y)
                break

            visibility[int(check_x * 100), int(check_y * 100)] = True

        return check_x, check_y, False

    def location_is_visible(self, location: V):
        return self.visibility[floor(location * 100)]

    def draw_sprites(self):
        """Draw all the worldsprites that are running around in the level on top of the level itself."""
        visible_sprites = [s for s in self.sprites if self.location_is_visible(s.location)]
        visible_sprites.sort(key = lambda s: self.camera.location.distance(s.location), reverse=True)

        if self.debug:
            for thesprite in self.sprites:
                rect = pygame.Rect((0, 0), (self.debug_view_scale, self.debug_view_scale))
                rect.center = floor(thesprite.location * self.debug_view_scale)
                self.surf.blit(
                    pygame.transform.smoothscale(thesprite.img,
                                                 (floor(self.debug_view_scale), floor(self.debug_view_scale))),
                                                 rect)
            return

        for thesprite in visible_sprites:
            angle_to_sprite = self.camera.location.bearing(thesprite.location)
            distance_to_sprite = self.camera.location.distance(thesprite.location)
            distance_to_sprite *= (cos(self.camera.rotation - angle_to_sprite) * 1)
            if distance_to_sprite <= 0.01: continue

            if thesprite.direction is not None:
                angle_to_sprite = clamp_angle_for_comparison(angle_to_sprite)
                perspective_width_factor = 1 - abs(angle_to_sprite / (pi / 2))
            else:
                perspective_width_factor = 1

            camera_relative_angle = (self.camera.rotation - angle_to_sprite)
            camera_relative_angle = clamp_angle_for_comparison(camera_relative_angle)

            screenx = ((self.fov/2) - camera_relative_angle) * self.pixels_per_radian
            l = 1 / distance_to_sprite * 600
            mid = self.h // 2
            floorheight    = mid + l/2
            ceilingheight  = mid - l/2
            vertical_range = ceilingheight - floorheight
            screeny = int(floorheight + vertical_range * thesprite.height)

            #pygame.draw.circle(self.surf, (64, 64, 255), (round(screenx), screeny), 8)
            l = min(l, 1024)
            try:
                scaledsprite = pygame.transform.smoothscale(thesprite.img,
                                                            (min(int(l * perspective_width_factor), 2048),
                                                             min(int(l), 2048)))
            except pygame.error:
                import traceback
                print(traceback.format_exc())
                continue

            rect = scaledsprite.get_rect()

            try:
                rect.center = screenx, screeny
            except TypeError:
                print(f"sprite way too huge x = {screenx}, y = {screeny}")

            scaledspritewidth = rect.width // 2
            relevant_zbuffer = self.zbuffer[round(screenx) - scaledspritewidth : round(screenx) + scaledspritewidth]
            occlusion = relevant_zbuffer < (distance_to_sprite)
            #print(relevant_zbuffer, distance_to_sprite)
            if occlusion.sum() != 0:
                #print(occlusion)
                lside = occlusion[:scaledspritewidth]
                rside = occlusion[scaledspritewidth:]
                area = scaledsprite.get_rect()
                area.left += sum(lside)
                area.right -= sum(rside)
                rect.x += sum(lside) - sum(rside)
            else:
                area = None

            self.surf.blit(scaledsprite, rect, area = area)

if __name__ == "__main__":
    from world import the_world as world

    camera = Camera((2, 1))

    pygame.display.init()
    r = WorldRenderer(camera, world, surface=pygame.display.set_mode((800,600)))

    pygame.key.set_repeat(100, 10)

    f = 0
    while True:
        for theevent in pygame.event.get():
            if theevent.type == pygame.KEYDOWN:
                if theevent.key == pygame.K_w:
                    camera.location += V.from_yaw(camera.rotation) * 0.1
                elif theevent.key == pygame.K_s:
                    camera.location -= V.from_yaw(camera.rotation) * 0.1

                if theevent.key == pygame.K_a:
                    camera.rotation -= 0.05
                elif theevent.key == pygame.K_d:
                    camera.rotation += 0.05

                if theevent.key == pygame.K_F7:
                    r.debug = not r.debug

                if theevent.key == pygame.K_RIGHTBRACKET:
                    r.debug_view_scale -= 2
                    print(r.debug_view_scale)
                elif theevent.key == pygame.K_LEFTBRACKET:
                    r.debug_view_scale += 2
                    print(r.debug_view_scale)

            elif theevent.type == pygame.MOUSEBUTTONUP:
                if theevent.button == 4:
                    r.fov += 0.025
                    r.radians_per_pixel = r.fov / r.w
                elif theevent.button == 5:
                    r.fov -= 0.025
                    r.radians_per_pixel = r.fov / r.w

        r.draw()
        pygame.display.flip()
        f+=1

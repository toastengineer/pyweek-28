import collections
import contextlib
import functools
import itertools
import math
import os
import sys
import time
import typing
import random

import controls
import gfx
from copy import deepcopy
from pathlib import Path

import pygame

import text_menu
import windowmanager
from vec2d import V

if typing.TYPE_CHECKING:
    import character

ESTIMATING_FUTURE = 0

@contextlib.contextmanager
def imagination():
    """Context manager used to globally set that we're running a simulation for AI purposes and not
    actually running the full game rules. Can be nested."""
    global ESTIMATING_FUTURE
    ESTIMATING_FUTURE += 1
    old_stdout = sys.stdout
    sys.stdout = open(os.devnull, "wt")
    yield
    sys.stdout = old_stdout
    ESTIMATING_FUTURE -= 1

class PrintToNotificationBox:
    def __init__(self):
        self.old_stdout = sys.stdout
        sys.stdout = self

    def restore_old_stdout(self):
        sys.stdout = self.old_stdout

    def write(self, text: str):
        if not text.isspace():
            text_menu.pop_up_notification(text)
            import gfx
            gfx.main_window.draw()
        self.old_stdout.write(text)

    def flush(self):
        pass

placeholder_image = pygame.image.load(str(Path("images", "placeholder.png")))

#Wanna use this for a background at some point
#text = itertools.cycle(itertools.chain.from_iterable(open(mod.__file__, "rt").read().replace("\n", "") for mod in list(sys.modules.values()) if hasattr(mod, "__file__") and Path(mod.__file__).is_file()))

class ScaledImageCache(dict):
    def __getitem__(self, key):
        img, size = key
        try:
            return super().__getitem__((id(img), size))
        except KeyError:
            return self.scale(img, size)

    def __setitem__(self, key, value):
        img, size = key
        super().__setitem__((id(img), size), value)

    def scale(self, surf, size):
        self[surf, size] = surf = pygame.transform.smoothscale(surf, size)
        return surf

scaled_image_cache = ScaledImageCache()


class BattleWindow(windowmanager.GfxWindow):

    def __init__(self, battle: 'BattleState'):
        super().__init__(gfx.TARGET_WORLD_VIEW_RESOLUTION, gfx.main_window.world_view_rect)
        self.battle = battle
        battle.expect_external_PC_actions = True
        self.shake = 0
        self.realrect = self.rect.copy()
        controls.bindings.push_modal(self)
        self.selected_enemy = None #type: character.Character

    def decorate(self):
        self.surf.fill((0,0,0))

        if self.shake > 0:
            k = math.ceil(self.shake)
            self.rect = self.realrect.move(random.randint(-k,k),
                                           random.randint(-k,k))
            self.shake -= (math.log10(self.shake)) + 1

        mouse_loc = V(*pygame.mouse.get_pos()) - V(self.rect.topleft)

        w, h = self.rect.size

        enemy_count         = len(self.battle.enemies)
        vertical_sections   = max(math.floor(math.pow(enemy_count, 1/3)), 1)
        horizontal_sections = math.ceil(enemy_count / vertical_sections)

        section_width =  math.ceil(w / horizontal_sections)
        section_height = math.ceil(h / vertical_sections)
        section_size = section_width, section_height

        top_left_corners_of_sections = ((section_width * x, section_height * y)
                                         for y in range(vertical_sections) for x in range(horizontal_sections))
        section_rects = (pygame.Rect(topleft, section_size) for topleft in top_left_corners_of_sections)

        self.selected_enemy = None
        try:
            for the_enemy, section in zip(self.battle.enemies, section_rects):
                selected = section.collidepoint(mouse_loc)

                self.surf.set_clip(section)
                if selected:
                    self.surf.fill(gfx.FGCOLOR)
                    self.selected_enemy = the_enemy

                image = getattr(the_enemy, "image", placeholder_image)

                bbox = image.get_bounding_rect()
                width_adjustment = min(1, section_width / bbox.width)
                height_adjustment = min(1, section_height / bbox.height)
                size_adjustment = min(width_adjustment, height_adjustment)

                img_width, img_height = image.get_rect().size
                img_width *= size_adjustment
                img_height *= size_adjustment

                bbox.size = int(img_width), int(img_height)

                bbox.center = section.center = section.center

                self.surf.blit(scaled_image_cache[image, bbox.size], bbox)

        finally:
            self.surf.set_clip(None)

    @controls.action("Get Info On Character", (pygame.K_RETURN, "MouseLeft"))
    def on_click(self, up):
        enemy = self.selected_enemy
        from gamestate import gamestate

        character = enemy or gfx.main_window.bottom_textbox.selected_character
        if not character: return

        menu = text_menu.PopUpMenu(prompt=character.stat_block(inventory=True), loop_until_non_callable=True,
                                   blocking=False, cancellable=True)

        menu["Inspect Inventory"] = text_menu.PopUpMenu({
            str(item): lambda: functools.partial(text_menu.pop_up_notification, item.__doc__)
            for item in character.inventory
        }, loop_until_non_callable=True, cancellable=True)
        menu["Inspect Inventory"]["Back"] = None

        if enemy:
            charname_width = max(len(c.name) for c in gamestate.party)

            def register_action(action):
                del menu["Attack"][f"{action.user:{charname_width}}: {action}"]
                self.battle.external_PC_actions[action.user] = action
                if len(self.battle.external_PC_actions) == len(list(pc for pc in gamestate.party if not pc.incapacitated)):
                    menu["Attack"].close()
                    menu.close()

            all_actions = list(itertools.chain.from_iterable(p.collect_battle_actions(self.battle)
                                                             for p in gamestate.party))
            menu["Attack"] = text_menu.PopUpMenu({
                f"{action.user:{charname_width}}: {action}" : functools.partial(register_action, action)
                for action in all_actions
                if getattr(action, "target", None) == enemy
                and self.battle.external_PC_actions.get(action.user, None) != action
            }, loop_until_non_callable = True, cancellable=True)
            menu["Attack"]["Back"] = None

        elif gfx.main_window.bottom_textbox.selected_character:
            def register_action(action):
                del menu["Choose Action"][str(action)]
                self.battle.external_PC_actions[action.user] = action
                raise text_menu.BreakOutOfMenu()

            menu["Choose Action"] = text_menu.PopUpMenu({
                str(action): functools.partial(register_action, action)
                for action in character.collect_battle_actions(self.battle)
            }, cancellable=True)
            menu["Choose Action"]["Back"] = None


        menu["Close"] = None

        menu()

class BattleState:
    def __init__(self, player_characters: 'list[character.Character]', enemies: 'list[character.Character]'):
        self.player_characters = player_characters
        self.enemies = enemies
        self.predecessor = None
        self.autobattle = False
        self.state = "Waiting for next turn"
        self.external_PC_actions : 'dict[character, character.BattleAction]' = dict()
        self.last_turn = None
        self.queued_actions = []

    @property
    def participants(self):
        return self.player_characters + self.enemies

    @property
    def active_participants(self):
        return [c for c in self.participants if not c.dead]

    def teams(self, me):
        """Returns my_team, enemy_team, as lists"""
        if me in self.player_characters:
            return self.player_characters, self.enemies
        elif me in self.enemies:
            return self.enemies, self.player_characters
        else:
            raise ValueError(f"{me} isn't part of either team")

    @property
    def is_over(self):
        return (not self.player_characters or not self.enemies or
                all(pc.incapacitated for pc in self.player_characters) or
                all(npc.incapacitated for npc in self.enemies))

    def __getstate__(self):
        retdict = self.__dict__.copy()
        retdict["predecessor"] = None
        return retdict

    def turn(self):
        if self.state == "Waiting for next turn":

            for the_character in self.participants:
                the_character.process_status_effects()

            self.last_turn = deepcopy(self)
            self.state = "Getting PC and enemy actions" if self.autobattle else "Getting enemy actions"

        if self.state == "Getting PC and enemy actions":
            self.queued_actions[:] = (the_character.get_next_battle_action(self.last_turn)
                       for the_character in self.last_turn.active_participants)
            self.state = "Executing actions"

        if self.state == "Getting enemy actions":
            for the_character in self.last_turn.enemies:
                if the_character.incapacitated: continue
                action = the_character.get_next_battle_action(self.last_turn)
                self.queued_actions.append(action)
            self.state = "Getting PC actions"

        if self.state == "Getting PC actions":
            active_pcs = set(pc for pc in self.player_characters if not pc.incapacitated)
            if set(self.external_PC_actions) >= active_pcs:
                self.queued_actions.extend(self.external_PC_actions.values())
                self.external_PC_actions.clear()
                self.state = "Executing actions"

        if self.state == "Executing actions":
            for the_action in self.queued_actions:
                the_action(battle=self)
            self.queued_actions.clear()
            self.state = "Waiting for next turn"

    def expand(self):
        """Return a list of all possible next states from this state."""



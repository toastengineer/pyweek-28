import os, sys

if sys.platform == "win32":
    os.environ["SDL_VIDEODRIVER"] = "directx"
os.environ["PYGAME_FREETYPE"]=""
os.environ["PYGAME_HIDE_SUPPORT_PROMPT"] = "hide"


import pygame

if __name__ == "__main__":
    pygame.init()
    pygame.key.set_repeat(500, 75)

    from gamestate import gamestate

    gamestate.run_game()

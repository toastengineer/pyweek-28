from collections import namedtuple
from math import floor, sin, cos, hypot, floor, ceil, atan2


class V(namedtuple('V', ['x', 'y'])):
    """2D vector"""

    def __new__(cls, x, y=None):
        try:
            return super().__new__(cls, *x)
        except TypeError:
            return super().__new__(cls, x, y)

    def __gt__(self, other):
        return self.x > other[0] and self.y > other[1]
    def __lt__(self, other):
        return self.x < other[0] and self.y < other[1]
    def __ge__(self, other):
        return self.x >= other[0] and self.y >= other[1]
    def __le__(self, other):
        return self.x <= other[0] and self.y <= other[1]

    def __add__(self, other):
        return V(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return V(self.x - other.x, self.y - other.y)

    def __mul__(self, other):
        if isinstance(other, V):
            raise NotImplementedError()
        else:
            return V(self.x * other, self.y * other)

    def __truediv__(self, other):
        return V(self.x / other, self.y / other)

    def __round__(self, n=None):
        return V(round(self.x, n), round(self.y, n))

    def __floor__(self):
        return V(floor(self.x), floor(self.y))

    def __floordiv__(self, other):
        return V(int(self.x//other), int(self.y//other))

    def __mod__(self, other):
        try:
            x, y = other
        except TypeError:
            x = y = other

        return V(self.x % x, self.y % y)

    def round_towards(self, other):
        """If other.x is positive, round self.x up, otherwise round down.
           If other.y is positive, round self.y up, otherwise round down.

           You can think of this as a "velocity" round, if you do
           grid_location = self.location.round_towards(self.velocity)
           it'll give you the nearest on-grid (rounded) point you were heading towards.
        """
        return V(
            ceil(self.x) if other.x > 0 else floor(self.x),
            ceil(self.y) if other.y > 0 else floor(self.y)
        )

    def angle_to(self, other):
        """
        Assuming this vector and the other are coincident lines
        return the degrees of separation between them.
             |
         self| theta
             |-----
               other
        """
        return atan2(other.y - self.y, other.x - self.x)

    def atan2(self):
        """Assuming this vector represents a direction, return the
        angular offset in radians this vector represents from (0, 1) (straight down)"""
        return atan2(self.x, self.y)

    def bearing(self, other):
        """Assuming this and other are points, return the
        angle you'd have to rotate a vector pointing straight down
        to get a vector pointing from this point to the the other."""
        return (other - self).atan2()

    @classmethod
    def from_yaw(cls, yaw):
        """Return a vector yaw radians clockwise from (0,1) (straight down). I.e.
        >>> V.from_yaw(radians(90))
        (1,0)"""
        return V(sin(yaw), cos(yaw))

    def magnitude(self):
        return hypot(*self)

    def distance(self, other):
        return (other - self).magnitude()

    def normalized(self):
        return self / self.magnitude()
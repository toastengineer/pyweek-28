import functools
import itertools
import math
import random
from pathlib import Path

import networkx as networkx
import numpy

import rendering
import text_menu
from vec2d import V

def neighbors(loc, worldshape):
    return {V(loc) + adj for adj in
            (V(2,0), V(0,2), V(-2,0), V(0,-2))
            if (0,0) <= (V(loc) + adj) < worldshape}


directions = ((V(2,0), V(0,2), V(-2,0), V(0,-2)))
def growing_tree(size, pickfunc = random.choice, loopiness = 0, startloc = None):
    if not isinstance(size, tuple): size = (size, size)
    w, h = size
    size = w * 2 - 1, h * 2 - 1
    world = numpy.ndarray(size, numpy.bool_)
    graph = networkx.Graph()
    world[:] = True

    if startloc is None: startloc = V(random.randint(0, w-1) * 2, random.randint(0, h-1) * 2)
    open = [startloc]
    visited = set()
    while open:
        the_loc = pickfunc(open)
        visited.add(the_loc)
        world[the_loc] = False


        candidate_neighbors = neighbors(the_loc, world.shape)
        candidate_neighbors -= visited

        if not candidate_neighbors:
            open.remove(the_loc)
            if random.random() <= loopiness:
                candidate_neighbors = neighbors(the_loc, world.shape)
            else:
                continue

        neighbor_to_connect = random.choice(list(candidate_neighbors))

        world[neighbor_to_connect] = False
        midpoint = (the_loc + neighbor_to_connect) // 2
        world[midpoint] = False

        graph.add_path((the_loc, midpoint, neighbor_to_connect))

        if neighbor_to_connect not in visited:
            open.append(neighbor_to_connect)
            visited.add(neighbor_to_connect)

    pad_adjusted_graph = networkx.Graph((a + V(1,1), b + V(1,1)) for a, b in graph.edges)
    return numpy.pad(world, 1, constant_values=(True,)), pad_adjusted_graph


the_world = [[True, True, True, True, True, True, True, True, True, True, True, True, True, True, True],
             [True, False, False, False, False, False, False, False, False, False, False, False, True, False, True],
             [True, False, False, False, False, False, False, False, False, False, False, False, True, False, True],
             [True, False, False, False, False, False, False, False, False, False, False, False, True, False, True],
             [True, False, False, False, True, False, False, False, False, True, False, False, False, False, True],
             [True, False, False, False, False, False, True, True, True, True, True, False, False, False, True],
             [True, False, False, False, False, False, True, False, False, False, False, False, False, False, True],
             [True, True, False, False, False, False, True, False, True, True, True, False, True, False, True],
             [True, False, False, False, False, False, True, False, False, False, False, False, False, False, True],
             [True, True, True, True, True, True, True, True, True, True, True, True, True, True, True]]

# world = [[True, True, True, True, True, True, True, True, True, True],
# [True, False, False, False, False, False, False, False, False, True],
# [True, False, False, False, False, False, False, False, False, True],
# [True, False, False, False, False, False, True, False, False, True],
# [True, False, False, False, False, False, False, True, False, True],
# [True, False, True, False, False, False, False, False, False, True],
# [True, False, False, True, False, False, False, False, False, True],
# [True, False, False, False, False, False, False, False, False, True],
# [True, True, True, True, True, True, True, True, True, True]]

class Entity(rendering.WorldSprite):
    img_path = Path()

    def __init__(self, location):
        super().__init__(str(self.img_path), location)

    @classmethod
    def place(cls, world):
        pass

    def on_player_touch(self, gamestate):
        pass

    def update(self, gamestate):
        pass

    def on_interact(self, gamestate):
        pass


class ExitDoor(Entity):
    height = 0.4
    direction = math.pi / 2
    img_path = Path("images", "exitdoor.png")
    def __init__(self):
        super().__init__((1.5, 1.1))

    def __str__(self):
        return "the exit door"

    @classmethod
    def place(cls, world):
        if world.floor == 0:
            world.sprites.append(cls())

    def on_player_touch(self, gamestate):
        if text_menu.pop_up_yes_no("Leave the Tower? Any items left inside will be lost!"):
            gamestate.current_level = None
        else:
            gamestate.camera.location = V(1.5, 1.5)

    def on_interact(self, gamestate):
        if text_menu.pop_up_yes_no("Leave the Tower? Any items left inside will be lost!"):
            gamestate.current_level = None

class LadderUp(Entity):
    height = 1.25
    img_path = Path("images", "ladderup.png")
    def __init__(self, location, level):
        super().__init__(location)
        self._next_floor = None
        self.next_ladder = None
        self.floor = level.floor
        self.level = level

    def __str__(self):
        return f"a ladder up to Floor {self.floor + 1}"

    @classmethod
    def place(cls, world):
        loc = world.end_loc + V(0.5, 0.5)
        world.sprites.append(cls(loc, world))

    @property
    def next_floor(self):
        if not self._next_floor:
            self._next_floor =  World(self.floor + 1)
            next_ladder_loc = self._next_floor.start_loc + V(0.5, 0.5)
            self.next_ladder = LadderDown(next_ladder_loc, self.level, self)
            self._next_floor.sprites.append(self.next_ladder)

        return self._next_floor

    def on_interact(self, gamestate):
        gamestate.current_level = self.next_floor
        gamestate.camera.location = self.next_ladder.location
        print(f"You climb the ladder to Floor {gamestate.current_level.floor}.")

class LadderDown(Entity):
    height = 0
    img_path = Path("images", "ladderdown.png")

    def __str__(self):
        return f"a ladder down to the previous floor"

    def __init__(self, location, prev_level, prev_ladder):
        self.prev_level = prev_level
        self.prev_ladder = prev_ladder
        super().__init__(location)

    def on_interact(self, gamestate):
        gamestate.current_level = self.prev_level
        gamestate.camera.location = self.prev_ladder.location
        print(f"You climb down the ladder to Floor {gamestate.current_level.floor}.")

class RoundRobinMultiplexer:
    def __init__(self, switch_every, *args):
        self.channels = args
        self.switch_every = switch_every
        self.cycler = itertools.cycle(range(switch_every * len(args)))
    def __call__(self, *args, **kwargs):
        idx = next(self.cycler) % len(self.channels)
        return self.channels[idx](*args, **kwargs)
    def __repr__(self):
        import inspect
        return ' '.join(inspect.getsource(f) for f in self.channels)

dfs = lambda x: x[-1]
bfs = lambda x: x[0]
fiftyfifty = lambda x: x[-1] if random.random() < 0.5 else x[0]
seventytwenty = lambda x: x[-1] if random.random() < 0.7 else x[0]
twentyseventy = lambda x: x[-1] if random.random() > 0.7 else x[0]
mid = lambda x: x[len(x)//2]
magicnumber = lambda x: x[int(len(x) * 0.845)]
rand = random.choice
sinlen = lambda x: x[math.floor(math.sin(len(x))*len(x))]
triangle = lambda x: x[int(random.triangular(0, 1, 0) * len(x))]
min_ = lambda x: min(x, key = lambda v: v.magnitude())
max_ = lambda x: max(x, key = lambda v: v.magnitude())

maze_styles = [dfs, bfs, triangle, magicnumber, min_, max_, mid, sinlen]
combos  = [RoundRobinMultiplexer(200, a, b) for a in maze_styles for b in maze_styles if a is not b]
combos2 = [functools.partial(
            lambda a, b, x: a(x) if random.random()<0.5 else b(x), a, b)
            for a in maze_styles for b in maze_styles]
maze_styles.extend(combos)
maze_styles.extend(combos2)
combos3 = [functools.partial(
            lambda a, b, x: a(x) if random.random()<0.5 else b(x), a, b)
            for a in combos for b in combos]
maze_styles.extend(combos3)
maze_style_cycler = itertools.cycle(maze_styles)

class World:
    def __init__(self, floor = 0):
        self.floor = floor
        maze_style = dfs if floor <= 1 else next(maze_style_cycler)#random.choice(maze_styles)
        loopiness = 0 if floor <= 1 else min(random.random() * min(floor / 150, 1), 1)
        size = max(5, round(2 * pow(floor, 0.5)))
        import inspect
        name = inspect.getsource(maze_style) if not isinstance(maze_style, RoundRobinMultiplexer) else repr(maze_style)
        print(name, loopiness)
        self.map, self.graph = growing_tree(size, maze_style, loopiness)
        self.shortest_paths = dict(networkx.all_pairs_shortest_path(self.graph))
        longest_shortest_path = max([self.shortest_paths[a][b] for a in self.graph.nodes for b in self.graph.nodes],
                                    key = len)
        self.start_loc = longest_shortest_path[0]
        self.end_loc = longest_shortest_path[-1]
        self.open_locations = set(map(V, numpy.transpose((self.map != True).nonzero())))
        self.adjacent_open_cells = numpy.pad(numpy.sum((self.map[1:-1,0:-2]==0, self.map[1:-1, 2:]==0, self.map[0:-2, 1:-1]==0, self.map[2:, 1:-1]==0),
                                             dtype=numpy.int8, axis=0), 1, constant_values=(0,))
        self.map &= (self.adjacent_open_cells != 4) #make any closed cells surrounded by open cells open
        self.open_locations_sorted_by_adjacent_cells = sorted(self.open_locations,
                                                              key = lambda x: self.adjacent_open_cells[x])
        self.sprites = []
        for the_entity_class in Entity.__subclasses__():
            the_entity_class.place(self)

        #What will get .on_interact if the player presses the interact button
        self.interaction_target = None

    def update(self, gamestate):
        for thesprite in self.sprites:
            thesprite.update(gamestate)

        #Sprites can kick us out of the map, so better check if it's still there
        if not gamestate.current_level:
            return

        player_loc = gamestate.camera.location
        for thesprite in (s for s in self.sprites if player_loc.distance(s.location) < 0.15):
            thesprite.on_player_touch(gamestate)


        interaction_candidates = (s for s in self.sprites if
                                  gamestate.world_renderer.location_is_visible(s.location) and
                                  player_loc.distance(s.location) <= 4)
        last_interaction_target = self.interaction_target
        try:
            self.interaction_target = min(interaction_candidates,
                                          key = lambda s: player_loc.distance(s.location))
            if self.interaction_target != last_interaction_target:
                print(f"You see {self.interaction_target}.")
        except ValueError: #no candidates
            self.interaction_target = None


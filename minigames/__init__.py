import difflib
import functools
import random
import time
from math import *

import pygame as p
import pygame.draw as d
import pygame.gfxdraw as g
import pygcurse
from pygame import Rect

import gfx, windowmanager

clock = p.time.Clock()
tick60 = functools.partial(clock.tick, 60)

GLOBAL_DIFFICULTY_MULTIPLIER = 2

from pygame import freetype
minigame_font = freetype.Font("B612-Bold.ttf", size=12)

#All minigames return a float from 0 to 1, 1 representing total success and 0 total failure.
#They take a float value representing difficulty - 1 = normal difficulty, higher = harder, lower = easier

def get_surface(size) -> p.Surface:
    win = p.display.get_surface()
    rect = Rect((0,0), size)
    rect.center = win.get_rect().center
    surf = win.subsurface(rect)

    surf.fill((0,0,0))
    d.rect(win, (128,128,128), rect.inflate(4, 4), 2)
    return surf

def show_prompt(mainsurface, prompt: str):
    """Given the surface get_surface gave you (just need it for its subsurface location)"""
    mainsurface_rect = mainsurface.get_rect(topleft = mainsurface.get_offset())
    text_rect = minigame_font.get_rect(prompt)
    text_box_rect = text_rect.inflate(25, 25)
    text_box_rect.midbottom = mainsurface_rect.midtop
    text_rect.center = text_box_rect.center
    d.rect(p.display.get_surface(), (0,0,0), text_box_rect)
    d.rect(p.display.get_surface(), (127,127,127), text_box_rect.inflate(4,4), 2)
    minigame_font.render_to(p.display.get_surface(), text_rect, prompt, fgcolor=gfx.FGCOLOR)

def get_pygcurse_surface(size = (80, 3)):
    """Size is in characters"""
    w, h = size
    w *= 8
    h *= 16
    s= pygcurse.PygcurseSurface(*size, font = gfx.FIXEDSYS, fgcolor = (230,230,230),
                                    windowsurface=get_surface((w, h)))
    s.fill() #needed for setting the bgcolor to work
    return s

def timed_input(s, time_limit):
    starttime = time.perf_counter()
    buffer = []

    def callbackfn(inputobj):
        time_left = time_limit - (time.perf_counter() - starttime)
        if time_left <= 0: raise OutOfTime()
        greenness = int((time_left / time_limit) * 150)
        s.bgcolor = (150 - greenness, greenness, 0)
        s.fill(None, bgcolor=(150 - greenness, greenness, 0))
        buffer[:] = inputobj.buffer

    try:
        result = s.input(callbackfn=callbackfn)
    except OutOfTime:
        result = ''.join(buffer)
    return result

def any_button():
    for theevent in p.event.get():
        if theevent.type == p.KEYDOWN or theevent.type == p.MOUSEBUTTONDOWN or theevent.type == p.JOYBUTTONDOWN:
            return True

class OutOfTime(BaseException):
    """You should never actually get this exception - it's used in some minigames to implement the timer."""

def test_your_strength(difficulty, prompt = None):
    difficulty *= 2.5
    s = get_surface((300, 100))
    if prompt: show_prompt(s, prompt)
    time_between_lights = min(1, 1 / difficulty)
    light1_time = time_between_lights
    light2_time = time_between_lights * 2
    light3_time = time_between_lights * 3
    light4_time = time_between_lights * 4
    starttime = time.perf_counter()
    required_response_time = 0.05 / difficulty

    c = lambda x: 255 if x else 40

    while True:
        s.fill((0,0,0))
        dt = time.perf_counter() - starttime

        c1 = 255 if dt >= light1_time else 50
        c2 = 255 if dt >= light2_time else 50
        c3 = 255 if dt >= light3_time else 50
        c4 = 255 if dt >= light4_time else 50

        #d.circle(s, (c1,0,0), (50, 50), 25)
        #d.circle(s, (c2,c2,0), (100, 50), 25)
        #d.circle(s, (c3,c3,0), (150, 50), 25)
        #d.circle(s, (0,c4,0), (250, 50), 50)
        g.filled_circle(s,  45, 50, 25, (c1,0,0))
        g.aacircle(s,  45, 50, 25, (c1, 0, 0))
        g.filled_circle(s, 100, 50, 25, (c2,c2,0))
        g.aacircle(s, 100, 50, 25, (c2, c2, 0))
        g.filled_circle(s, 155, 50, 25, (c3,c3,0))
        g.aacircle(s, 155, 50, 25, (c3, c3, 0))
        g.filled_circle(s, 250, 50, 45, (0,c4,0))
        g.aacircle(s, 250, 50, 25, (0, c4, 0))

        if any_button():
            if dt >= light4_time:
                reaction_time = dt - light4_time
                score = min(1, max(0, (required_response_time / reaction_time)))
                return score
            elif dt >= light3_time:
                #False starts earn you a 0
                return 0

        #clock.tick(60)
        p.display.flip()

def drunk_darts(difficulty, prompt = None):
    difficulty *= 1.5
    difficulty **= 2
    s = get_surface((300, 100))
    if prompt: show_prompt(s, prompt)
    width = s.get_width()
    target = width / 2

    bg = p.Surface(s.get_size())

    for x in range(0, width, 4):
        d.line(bg, (127, 127, 127), (x, 40), (x, 60))

    d.line(bg, (255, 255, 0), (round(target), 20), (round(target), 80))

    while True:
        s.fill((0,0,0))
        t = time.perf_counter()
        s.blit(bg, (0,0))

        score = 1 - abs(sin(t * difficulty))
        location = int(target + sin(t * difficulty) * target)
        greenness = int(255 * score)
        d.line(s, (255-greenness, greenness, 0), (location, 20), (location, 80))
        p.display.flip()

        if any_button():
            return score
#Taken from http://clagnut.com/blog/2380/#Longer_pangrams_in_English_.28in_order_of_fewest_letters_used.29
#(I skipped some because there were a lot)
pangrams = [
    "Nymphs blitz quick, vex dwarf, jog.",
    "Big fjords vex quick waltz nymph.",
    "Bawds jog, flick quartz, vex nymph.",
    "Glib jocks quiz nymph to vex dwarf.",
    "Bright vixens jump; dozy fowl quack.",
    "Quick zephyrs blow, vexing daft Jim.",
    "Sphinx of black quartz, judge my vow!",
    "Both fickle dwarves jinx my pig quiz.",
    "Fat hag dwarves quickly zap jinx mob.",
    "Public junk dwarves quiz mighty fox.",
    "How quickly daft jumping zebras vex.",
    "Two driven jocks help fax my big quiz.",
    "Jack, love my big wad of sphinx quartz!",
    "Do wafting zephyrs quickly vex Jumbo?",
    "Go, lazy fat vixen; be shrewd, jump quick.",
    "Jackdaws love my big sphinx of quartz",
    "Fickle jinx bog dwarves spy math quiz.",
    "Five hexing wizard bots jump quickly.",
    "Quick fox jumps nightly above wizard.",
    "Vamp fox held quartz duck just by wing.",
    "Five quacking zephyrs jolt my wax bed.",
    "The five boxing wizards jump quickly.",
    "Jackdaws love my big sphinx of quartz.",
    "Cozy sphinx waves quart jug of bad milk.",
    "My ex pub quiz crowd gave joyful thanks.",
    "A very bad quack might jinx zippy fowls.",
    "Pack my box with five dozen liquor jugs.",
    "Few quips galvanized the mock jury box.",
    "Sex-charged fop blew my junk TV quiz.",
    "Jack fox bids ivy-strewn phlegm quiz.",
    "The jay, pig, fox, zebra and my wolves quack!",
    "Blowzy red vixens fight for a quick jump.",
    "Sex prof gives back no quiz with mild joy.",
    "The quick brown fox jumps over a lazy dog.",
    "Boxers had zap of gay jock love, quit women.",
    "Quest judge wizard bonks foxy chimp love.",
    "Quizzical twins proved my hijack-bug fix.",
    "Fix problem quickly with galvanized jets.",
    "Hick dwarves jam blitzing foxy quip.",
    "Waxy and quivering, jocks fumble the pizza.",
    "Fake bugs put in wax jonquils drive him crazy.",
    "Big dwarves heckle my top quiz of jinx.",
    "My jocks box, get hard, unzip, quiver, flow.",
    "When zombies arrive, quickly fax judge Pat.",
    "A wizard’s job is to vex chumps quickly in fog.",
    "Heavy boxes perform quick waltzes and jigs.",
    "My faxed joke won a pager in the cable TV quiz show.",
    "Twelve ziggurats quickly jumped a finch box.",
    "Prating jokers quizzically vexed me with fibs.",
    "Amazingly few discotheques provide jukeboxes.",
    "The lazy major was fixing Cupid’s broken quiver.",
    "Foxy diva Jennifer Lopez wasn’t baking my quiche.",
    "The quick onyx goblin jumps over the lazy dwarf.",
    "\"Who am taking the ebonics quiz?\", the prof jovially axed.",
    "By Jove, my quick study of lexicography won a prize.",
    "As quirky joke, chefs won’t pay devil magic zebra tax.",
    "Big July earthquakes confound zany experimental vow.",
    "My girl wove six dozen plaid jackets before she quit.",
    "Six big devils from Japan quickly forgot how to waltz.",
    "Foxy parsons quiz and cajole the lovably dim wiki-girl.",
    "A very big box sailed up then whizzed quickly from Japan.",
    "Battle of Thermopylae: Quick javelin grazed wry Xerxes.",
    "Jack quietly moved up front and seized the big ball of wax.",
    "The wizard quickly jinxed the gnomes before they vaporized.",
    "A quivering Texas zombie fought republic linked jewelry.",
    "Zelda might fix the job growth plans very quickly on Monday.",
    "Quirky spud boys can jam after zapping five worthy Polysixes.",
    "Joke's on you, this was edutainment all along!"
]
def typing_tutor(difficulty, prompt = None):
    difficulty *= GLOBAL_DIFFICULTY_MULTIPLIER
    s = get_pygcurse_surface()
    if prompt: show_prompt(s.surface, prompt)
    sample = random.choice(pangrams)
    time_limit = 0.3 * len(sample) / difficulty
    s.write(sample +"\n")

    result=timed_input(s, time_limit)

    matcher = difflib.SequenceMatcher(None, sample, result)
    return matcher.ratio()

def quick_maths(difficulty, prompt = None):
    difficulty *= GLOBAL_DIFFICULTY_MULTIPLIER
    s = get_pygcurse_surface()
    if prompt: show_prompt(s.surface, prompt)

    opcount = min(max(2, int(difficulty + 2)), 10)
    summands = [random.randint(-9,9) for _ in range(opcount)]
    answer = sum(summands)

    time_limit = 10 / difficulty
    s.write(' + '.join(str(x) for x in summands) +"\n")

    result = timed_input(s, time_limit)

    try:
        result = int(result)
    except ValueError:
        return 0

    try:
        return 1 - 1 / abs(answer - result)
    except ZeroDivisionError:
        return 1

if __name__ == "__main__":
    busted_interpretkeyevent = pygcurse.interpretkeyevent
    def interpretkeyevent(keyEvent):
        try:
            if keyEvent.unicode and keyEvent.unicode.isalnum():
                return keyEvent.unicode
            else:
                return busted_interpretkeyevent(keyEvent)
        except AttributeError:
            return busted_interpretkeyevent(keyEvent)
    pygcurse.interpretkeyevent = interpretkeyevent

    p.init()
    p.display.set_mode((1280, 720))

    while True:
        print(f"score={quick_maths(1)}")
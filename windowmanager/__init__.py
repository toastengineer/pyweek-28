import inspect
import sys
import warnings
from typing import Union, Tuple, SupportsInt, List

import pygcurse
from pygame import Rect, Surface
import pygame

import gfx

int_pair = Tuple[SupportsInt, SupportsInt]
int_pair_or_rect = Union[Tuple[SupportsInt, SupportsInt], Rect]

windowstack: List['GfxWindow'] = []

class GfxWindow:

    def __init__(self, size : Union[int_pair, None] = None, location : Union[int_pair_or_rect, None] = None,
                 subsurface = False, border = True):
        """Location can be a Rect(), in which case size is ignored. Size can be provided without location,
        in which case the window will be centered on the middle of the screen.

        If subsurface is True, the window's backing surface will be a view on to the display surface itself -
        this is usually a bad idea - it's for minigames which take total control of rendering themselves.
        If subsurface is not True, the window will be blitted when windowmanager.draw_windows() is called.
        subsurface based windows will not receive .decorate() calls."""
        #For debugging
        caller = inspect.getframeinfo(inspect.stack()[1][0])
        self.created_at = f"{caller.filename}:{caller.lineno}"

        win = pygame.display.get_surface()

        if not isinstance(location, Rect):
            if size is None: raise ValueError("Must specify size if location is not a Rect or is left implicit")
            if location is None:
                location = Rect((0,0), size)
                location.center = win.get_rect().center
            else:
                x, y = map(int, location)
                location = Rect((x, y), size)

        self.rect = location

        if subsurface: self.surf = win.subsurface(self.rect)
        else:          self.surf = Surface(self.rect.size)
        self.subsurface = subsurface

        if not subsurface:
            windowstack.append(self)
        self.border = border

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def close(self):
        try:
            windowstack.remove(self)
        except ValueError:
            pass

    def decorate(self):
        """Meant to be overriden by subclasses - gets called right before the window
        is blitted to the screen."""

    def __repr__(self):
        return f"{type(self).__name__}({self.rect.topleft}, {self.rect.size})"

class TerminalWindow(GfxWindow):
    """Text terminal window, used by menus and text-based minigames. Size is in 8x16 characters,
    not pixels - location is still in pixels."""

    def __init__(self, size : int_pair = (80, 3), location : Union[int_pair_or_rect, None] = None,
                 subsurface = False, pxsize = None, border = True):
        if pxsize is not None:
            w, h = map(int, pxsize)
            w //=8; h //= 16
        else:
            w, h = map(int, size)

        super().__init__((w * 8, h * 16), location, subsurface, border)
        self.pygsurf = pygcurse.PygcurseSurface(w, h, gfx.FIXEDSYS,
                                                   windowsurface = self.surf)
        #Setting bgcolor on the terminal doesn't work unless you do this first
        self.pygsurf.fill()
        if not subsurface:
            self.pygsurf.autoupdate = False
            self.pygsurf.autodisplayupdate = False

    def decorate(self):
        self.pygsurf.update()


def draw_windows(target_surface: Surface):


    live_windows = []
    for the_window in windowstack:
        if sys.getrefcount(the_window) == 2: #the list, and the_window here
            warnings.warn(f"Orphan window defined @ {the_window.created_at}", RuntimeWarning)
            continue

        #draw frame - should this be a method on the window?
        if the_window.border:
            pygame.draw.rect(target_surface, (127,127,127), the_window.rect.inflate(2, 2), 2)
        the_window.decorate()
        target_surface.blit(the_window.surf, the_window.rect)
        live_windows.append(the_window)

    windowstack[:] = live_windows
    #pygame.event.pump()
    pygame.display.flip()



import sys
import threading
import time
from pathlib import Path
import pygame
import pygcurse

class ChunkedMusicPlayer:
    """Handles playing back the town theme."""""

    def __init__(self, tracks_dir = "musictest", requested_track = 0):
        self.tracks_dir = Path(tracks_dir)
        self.tracks = []
        self.tracknames = {}
        self.chunknames = {}
        self.chunk_to_track = {}
        for thesubdir in sorted(self.tracks_dir.iterdir()):
            if not thesubdir.is_dir(): continue
            thetrack = []
            self.tracknames[id(thetrack)] = thesubdir.name
            self.tracks.append(thetrack)
            for theclippath in sorted(thesubdir.iterdir()):
                theclip = pygame.mixer.Sound(str(theclippath))
                thetrack.append(theclip)
                self.chunk_to_track[theclip] = thetrack
                self.chunknames[theclip] = theclippath.name

        self._requested_track = requested_track
        self.next_chunk_idx = 0
        self.queued_chunk_idx = None
        self.stop_thread = False
        self.thread = None

        self.channel = pygame.mixer.find_channel()

    @property
    def requested_track(self):
        if callable(self._requested_track):
            return self._requested_track()
        else:
            return self._requested_track
    @requested_track.setter
    def requested_track(self, x):
        self._requested_track = x
        self.queue_chunk()

    def queue_chunk(self):
        track = self.tracks[self.requested_track]
        queued = self.channel.get_queue()

        if queued is None:
            self.next_chunk_idx %= len(track)
            chunk = track[self.next_chunk_idx]
            self.queued_chunk_idx = self.next_chunk_idx
            self.next_chunk_idx += 1
        elif queued not in track:
            chunk = track[self.queued_chunk_idx % len(track)]
        else:
            return 0.01

        self.channel.queue(chunk)
        length = chunk.get_length()
        if length: return length
        else:      return 0.01

    def play(self):
        self.queue_chunk()
        self.queue_chunk()

    def stop(self, fadeout = 0):
        self.stop_thread = True
        if not self.thread:
            self.channel.fadeout(1000)
            self.channel.stop()
        self.next_chunk_idx = 0

    def _thread_target(self):
        while not self.stop_thread:
            time.sleep(self.queue_chunk() / 2)
        self.channel.fadeout(1000)
        self.channel.stop()

    def spawn_thread(self):
        """Spawn a thread to queue up the next segment automatically."""
        self.stop_thread = False
        self.queue_chunk()
        self.thread = threading.Thread(target=self._thread_target, daemon=True, name=self.tracks_dir)
        self.thread.start()

class DoubleLayerMusicPlayer:
    """For playing music where, for example, you have an "explore" theme, and then
    when you get in to a battle, a separate track plays over it to create the "battle"
    theme, smoothly fading in and out."""

    def __init__(self, a_path, b_path):
        self.a = pygame.mixer.Sound(str(a_path))
        self.b = pygame.mixer.Sound(str(b_path))
        self.b_should_be_playing = False
        self.b_volume = 0.0001
        self.playing = False
        self.b_fader_thread = threading.Thread(target=self.b_fader, daemon=True).start()

    def b_fader(self):
        while True:
            if self.playing:
                if self.b_should_be_playing:
                    self.b_volume += 0.01
                else:
                    self.b_volume -= 0.01
                self.b_volume = max(0, min(1, self.b_volume))
                self.b.set_volume(self.b_volume)
            time.sleep(0.01 if self.playing else 0.1)

    def play(self):
        if not self.playing:
            self.playing = True
            self.a.play(-1)
            self.b.play(-1)
            self.b.set_volume(0.0001)

    def stop(self):
        self.playing = False
        self.a.fadeout(1)
        self.b.fadeout(1)
        #self.b_fader_thread.join()

if __name__ == "__main__":
    pygame.init()
    pygame.display.set_mode((10,10))

    p = DoubleLayerMusicPlayer(Path("music", "lv1", "a.ogg"), Path("music", "lv1", "b.ogg"))
    p.play()

    clock = pygame.time.Clock()
    while True:
        for theevent in pygame.event.get():
            if theevent.type == pygame.KEYUP:
                p.b_should_be_playing = not p.b_should_be_playing
        print(p.b.get_volume())
        clock.tick(60)


    w = pygcurse.PygcurseWindow(font=pygame.font.Font("FSEX300.ttf", 16))
    m = ChunkedMusicPlayer()
    m.play()
    w.autoupdate = False
    while True:
        for theevent in pygame.event.get():
            if theevent.type == pygame.KEYDOWN:
                if theevent.key == pygame.K_RIGHT:
                    m.requested_track = min(m.requested_track + 1, len(m.tracks) - 1)
                elif theevent.key == pygame.K_LEFT:
                    m.requested_track = max(m.requested_track - 1, 0)
                elif theevent.key == pygame.K_r:
                    m.stop()
                    m.play()
            elif theevent.type == pygame.QUIT:
                sys.exit()

        m.queue_chunk()

        w.fill()

        w.putchars("Playing: ", 0, 0)
        w.putchars(m.tracknames[id(m.chunk_to_track[m.channel.get_sound()])], 0, 1)
        w.putchars(m.chunknames[m.channel.get_sound()], 0, 2)

        w.putchars("Next:", 0, 4)
        try:
            w.putchars(m.tracknames[id(m.chunk_to_track[m.channel.get_queue()])], 0, 5)
            w.putchars(m.chunknames[m.channel.get_queue()], 0, 6)
        except KeyError:
            pass

        w.putchars("Left and right arrow keys to switch tracks, R to restart playback",0, 9)

        w.update()

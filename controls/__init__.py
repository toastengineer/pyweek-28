import collections
import inspect
from inspect import isfunction, ismethod

import pygame

mouse_button_map = {
    pygame.BUTTON_LEFT: "MouseLeft", pygame.BUTTON_RIGHT: "MouseRight",
    pygame.BUTTON_WHEELUP: "WheelUp", pygame.BUTTON_WHEELDOWN: "WheelDown",
    pygame.BUTTON_MIDDLE: "WheelClick"}

class Bindings:
    """Objects can bind their methods to keys/mouse buttons/gamepad buttons with
    this object. Use the action() decorator to mark functions and methods as actions,
    and then use .register(yourobj) to register them to receive callbacks when the key bound
    to that action is pressed, or set .modal to your object to make only it receive its
    callbacks.

    Key callbacks are called with False for key down and True for key up.
    """

    key_to_actions = collections.defaultdict(set)
    action_to_callable = {}
    modal_stack = []

    def dispatch(self):
        if self.modal_stack: action_to_callable = self.get_actions(self.modal_stack[-1])
        else:                action_to_callable = self.action_to_callable

        if not action_to_callable: return

        for the_event in pygame.event.get():
            up = the_event.type in (pygame.KEYUP, pygame.MOUSEBUTTONUP)
            #mousewheel rolling never produces an "up"
            for the_action in self.match_event_to_actions(the_event):
                callee = action_to_callable.get(the_action, lambda: None)
                #Might be a callable, might be a tuple of (object, methodname)
                if not callable(callee): callee = getattr(*callee)
                if inspect.Signature.from_callable(callee).parameters:
                    callee(up)
                #If the callee doesn't differentiate between keyup and down, only give it keydowns
                elif not up or getattr(the_event, "button", None) in (pygame.BUTTON_WHEELUP, pygame.BUTTON_WHEELDOWN):
                    callee()


    def match_event_to_actions(self, the_event):
        if hasattr(the_event, "key"):
            return self.key_to_actions.get(the_event.key, [])

        elif the_event.type == pygame.MOUSEBUTTONUP:
            try:             return self.key_to_actions[mouse_button_map[the_event.button]]
            except KeyError: return []

        #Joystick support would go here

        return []

    def register(self, obj):
        if isfunction(obj) and hasattr(obj, "action_name"):
            self.action_to_callable[obj.action_name] = obj
        else:
            for the_attr in dir(obj):
                attr_value = getattr(obj, the_attr)
                if ismethod(attr_value) and hasattr(attr_value, "action_name"):
                    self.action_to_callable[attr_value.action_name] = (obj, the_attr)

    @staticmethod
    def get_actions(obj):
        actions = {}
        for the_attr in dir(obj):
            attr_value = getattr(obj, the_attr)
            if ismethod(attr_value) and hasattr(attr_value, "action_name"):
                actions[attr_value.action_name] = (obj, the_attr)
        return actions

    def push_modal(self, modal):
        """Make the given object modal, meaning only it receives actions. The modal
        object does not need to be .register()-ed, but it won't hurt anything if it is
        both registered and also modal.

        There is a stack of modal objects - making an object modal while another object
        already is makes your object modal, but as soon as pop_modal is called on it,
        the previous object will go back to being the modal object."""
        self.modal_stack.append(modal)

    def pop_modal(self, modal = None):
        """If an object is given, remove it from the modal object stack. Otherwise, pop the top
        off of the modal object stack."""
        if modal is None:
            self.modal_stack.pop()
        else:
            self.modal_stack.remove(modal)

bindings = Bindings()

def action(action_name: str, default=None):
    """Designed to be used as a decorator, making the decorated function an action
    that can be bound to a key. A default key/button or list thereof can be given
    to bind to the action.

    @action("Move Selection Up", (pygame.K_up, "MousewheelUp"))
    def selection_up()
        ...

    This DOES NOT do the binding itself - it just marks the callable as an action
    that CAN be bound to a key. Tell the bindings object about it or the object it's
    a method of with controls.bindings.register() or modal()"""

    if default:
        try:
            if isinstance(default, str): raise TypeError
            for thekey in default:
                Bindings.key_to_actions[thekey].add(action_name)
        except TypeError:
            Bindings.key_to_actions[default].add(action_name)
    def inner(f):
        f.action_name = action_name
        return f
    return inner
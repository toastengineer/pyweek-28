TO PLAY:
Run main.exe

Or,

pip install -r requirements.txt
python main.py

Requires Numba - seemed pretty easy to set up, but then so did scipy and I got
a ton of flak for including that last time :P

------------------------------------------------------------------------------------------------------------------------

In menus:
Select options with mouse
    In some menus you can use arrow keys and enter, or mousewheel (up & down, and wheel-click to select.) Unfortunately,
    many menus are buggy.

In the 3D world:
WASD to move.
Esc. to show menu
Space to pause
Right click or TAB to interact with objects in the world

F5 toggles locking your view to cardinal directions + diagonals. You may or may not like this
F6 toggles locking your position to the cell grid. This, you probably won't like.

------------------------------------------------------------------------------------------------------------------------

Ancient prophesies foretold of the tower that rose out of the ground one day.
They said that legendary adventurers from across the land would unite, to face
its challenges, and die like the chumps they are.

It's not really causing anyone any problems, it's just full of treasure and monsters
for some reason. There's even a nice little town built up around it.  I mean, there's
_probably_ something awful at the top, but no-one worries about  that and the thing
is like a thousand floors high, no-one's been up past the first hundred.

The layout of the floors is different for every group who go in, and there's always
more loot to be had, so really, Base Town is the place to be if you're looking to get
rich quick and aren't afraid of a few agitated skeletons.

Go in. Get the money. Don't die.

-------------------------------------------------------------------------------
...is what I wish I could say happened in this game. Unfortunately I ran out of
time. I absolutely do intend to finish it properly though - go to
toastengineer.itch.io
and follow me on there if you think this game concept sounds cool and you want
to see the finished version.

Currently, most mechanics just don't work. Character class, for example,
doesn't actually do anything.

Arguably the biggest thing is that the "run away" option is completely broken.
Run away * JUST DELETES THAT CHARACTER *. So don't do that. Unless you forgot
to equip that character with a weapon, then you don't have any other option.
Whoops!

The way the battles are SUPPOSED to work is that both attacking and getting
attacked costs you stamina. You can make your attacks more efficient by
doing well at the minigames that pop up when you make an attack. As you run
low on stamina, it gets harder to keep yourself from getting hit by the enemy.
Getting hit makes you bleed, which kills you relatively quickly if you don't have
a medkit. Lucky thing medkits are bugged and don't get consumed when you use them,
huh?

There was SUPPOSED to be a dynamic of risk vs. reward, with overextending
too much being punished with character death, but there always being a little
more treasure behind every corner. Unfortunately, treasure never got implemented,
the hallways are empty. Still, the minigames are fun. Ah well.

Honestly, the Preparation theme Tieff wrote was worth it all by itself. :)

By TOASTEngineer. Music by tieff.
All images from thenounproject.com, CC-BY
https://creativecommons.org/licenses/by/3.0/us/legalcode

https://thenounproject.com/search/?q=ladder&i=2285526
https://thenounproject.com/term/doors/1609398/


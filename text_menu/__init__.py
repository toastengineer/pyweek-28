
#@TODO: Commonize this /w the same damn code in minigames, move it in to gfx
import itertools

import pygame
import pygcurse

import controls
import gfx
import windowmanager
from vec2d import V

def get_surface(size) -> 'tuple[V, pygame.Surface]':
    win = pygame.display.get_surface()
    rect = pygame.Rect((0,0), size)
    rect.center = win.get_rect().center
    surf = win.subsurface(rect)

    surf.fill((0,0,0))
    pygame.draw.rect(win, (128,128,128), rect.inflate(3, 3), 3)
    return V(rect.topleft), surf

def get_pygcurse_surface(size = (80, 3)):
    """Size is in characters"""
    w, h = size
    w *= 8
    h *= 16
    origin, surf = get_surface((w, h))
    s = pygcurse.PygcurseSurface(*size, font = gfx.FIXEDSYS, fgcolor = (230,230,230),
                                    windowsurface=surf)
    s.fill() #needed for setting the bgcolor to work
    return origin, s

def pop_up_prompt(prompt):
    """Print a prompt and get a string from the user."""
    lines  = prompt.split('\n')
    width  = max( len(l) for l in lines )
    width  = max(width, 80)
    height = len(lines) + 2

    with windowmanager.TerminalWindow((width, height)) as s:
        controls.bindings.push_modal(s)

        from gamestate import gamestate
        s.pygsurf.write(prompt)
        pygame.event.pump()
        text = s.pygsurf.input(maxlength = 80, callbackfn = lambda _: gamestate.frame())

        controls.bindings.pop_modal(s)
    return text

def pop_up_yes_no(prompt):
    """Ask a question, get True or False"""
    return PopUpMenu(Yes = True, No = False, prompt = prompt)()

def pop_up_notification(notification):
    """Show a message and make the user interact with it to close it.
    They still wont' read it, but they can't say you didn't try."""
    return PopUpMenu(Okay = False, prompt = notification)()


class BreakOutOfMenu(BaseException):
    pass

class PopUpMenuWindow(windowmanager.TerminalWindow):
    
    def __init__(self, menu : 'PopUpMenu'):
        self.menu = menu

        #It's possible to add options while the window is up, but it won't resize.
        #Maybe I should make it do so?
        width = max( len(k) for k in self.menu.keys() )
        if self.menu.prompt:
            width_of_prompt = max( len(l) for l in self.menu.prompt.split('\n') )
            width = max(width, width_of_prompt)
        width += 1

        height = len(self.menu)
        if self.menu.prompt:
            height += list(self.menu.prompt).count('\n') + 1

        super().__init__((width, height))


    def decorate(self):
        self.pygsurf.fill()
        self.pygsurf.cursor = [0, 0]

        if self.menu.prompt:
            self.pygsurf.write(self.menu.prompt)

        yorigin = self.pygsurf.cursory

        label_ys = {}
        for y, the_label in enumerate(self.menu):
            y += yorigin
            label_ys[y] = the_label
            if the_label is self.menu.currently_selected_key:
                self.pygsurf.putchars(f" {the_label}\n", fgcolor=(0,0,0), bgcolor=(225,225,225), y=y)
            else:
                self.pygsurf.putchars(the_label + "\n", y=y)

        #This'll go away when I move over to pyconsolegraphics and it handles mouse stuff itself
        if controls.bindings.modal_stack[-1] is self.menu:
            mouse_loc = V(*pygame.mouse.get_pos()) - V(self.rect.topleft)
            _, mousey = self.pygsurf.getcoordinatesatpixel(*mouse_loc)
            if mousey is not None:
                try:
                    self.menu.currently_selected_key = label_ys[mousey]
                except KeyError:
                    pass

        super().decorate()


class PopUpMenu(dict):
    """Modal dialog that appears in the center of the screen, presenting options (keys) to the user
    and returning the value mapped to the key that was picked, UNLESS the value is callable in which
    case it will be called and its return value will be returned. Arguments to the menu will be passed
    through to the callable value. This can be used to nest menus.

    If dontcall is true, this behavior is overridden and it just returns the callable.

    If only keyword arguments are provided, converts _s to spaces in the keys.

    If noreturn is True, does not return the result and just shows options forever. Break out by raising an
    exception."""

    def __init__(self, *args, loop_until_non_callable = False, prompt = None, dontcall = False, blocking = True,
                 initial_selection = None, cancellable = False, **kwargs):
        if kwargs and not args:
            kwargs = {k.replace("_", ' ') : v for k, v in kwargs.items()}
        if len(args) == 1 and not isinstance(args[0], dict):
            args = ({str(x) : x for x in args[0]},)
        super().__init__(*args, **kwargs)
        self.loop_until_non_callable = loop_until_non_callable
        self.prompt = prompt
        if self.prompt and self.prompt[-2:] != "\n\n": self.prompt += "\n\n"
        self.dontcall = dontcall
        self.blocking = blocking
        self.currently_selected_key = None
        self.selection = None
        self.window = None
        if initial_selection not in self.keys() and initial_selection in self.values():
            try:
                initial_selection = {v:k for k, v in self.items()}[initial_selection]
            except TypeError: raise TypeError("Can't provide value as initial_selection if values aren't hashable")
        self.initial_selection = initial_selection
        self.cancellable = cancellable

    def __call__(self, *args, **kwargs):
        self.args, self.kwargs = args, kwargs

        if self.window: #We're already active
            raise ValueError("Popup menu is already up!")

        self.window = PopUpMenuWindow(self)
        self.currently_selected_key = self.initial_selection
        if self.initial_selection is not None:
            self.selection = self[self.currently_selected_key]
        else:
            self.selection = None
        controls.bindings.push_modal(self)

        if self.blocking:
            from gamestate import gamestate
            while self.window: #.close() sets window to None, .close() gets called by .select()
                gamestate.frame()

            return self.selection

    @controls.action("Menu Selection Up", (pygame.K_UP, pygame.K_w, "WheelUp"))
    def selection_up(self):

        try: idx = list(self).index(self.currently_selected_key)
        except ValueError: idx = 0

        idx -= 1
        idx %= len(self)
        self.currently_selected_key = list(self)[idx]

    @controls.action("Menu Selection Down", (pygame.K_DOWN, pygame.K_s, "WheelDown"))
    def selection_down(self):
        try: idx = list(self).index(self.currently_selected_key)
        except ValueError: idx = -1

        idx += 1
        idx %= len(self)
        self.currently_selected_key = list(self)[idx]

    @controls.action("Menu Select", (pygame.K_e, pygame.K_RETURN, pygame.K_KP_ENTER, "MouseLeft", "WheelClick"))
    def select(self, button_up = True):
        if not button_up: return
        if self.currently_selected_key not in self:
            return

        result = self[self.currently_selected_key]

        if callable(result) and not self.dontcall:
            try:
                result = result(*self.args, **self.kwargs)
            except BreakOutOfMenu:
                #If we don't have loop_until_non_callable on, we assume we're inside another menu that does, so we
                #propagate the exception up to break out until it breaks out of a looping menu.
                self.close()
                if not self.loop_until_non_callable: raise
            else:
                if self.loop_until_non_callable: return

        self.selection = result
        self.close()

    @controls.action("Menu Close", (pygame.K_ESCAPE, "MouseRight"))
    def cancel(self, _):
        if not self.cancellable: return
        self.selection = None
        self.close()

    def close(self):
        if self.window is not None:
            self.window.close()
        self.window = None
        try:
            controls.bindings.pop_modal(self)
        except ValueError:
            pass #must've already been closed

if __name__ == "__main__":
    pygame.init()
    pygame.display.set_mode((1280, 720))

    pygcurse.calcfontsize = lambda x: (8, 16)

    m = PopUpMenu(
        foooooooooo = lambda: print("foo"),
        bar = lambda: print("bar"),
        baz = PopUpMenu(xyzzy = "xyzzy", plugh = "plugh", frotz = "frotz"),
        a = 'a',
        b='a',
        c='a',
        d='a',
        e='a',
        f='a',
        g='a',
        h='a',
        quit = "done!",
        loop_until_non_callable = True
    )
    print(m())















import math
import random
import time
import traceback
import typing
from pathlib import Path

import pygame

import battle
import character
import controls
import gfx
import musicplayer
import rendering
import text_menu
import town
import windowmanager
from vec2d import V


class ControllableCamera(rendering.Camera):

    def __init__(self):
        super().__init__()
        self.velocity = V(0,0)
        self.accel = 0 #how fast are we speeding up in the direction we're facing?
        self.angvel = 0
        self.angaccel = 0
        #Should the camera automatically re-orient itself to one of the 8 cardinal/intercardinal
        #directions? (N S E W NW NE SW SE)
        self.snap_rotation_to_grid = False
        self.snap_location_to_grid = False
        controls.bindings.register(self)

    def update(self, map):
        if self.accel:
            self.velocity += (V.from_yaw(self.rotation) * self.accel)
        else:
            if self.snap_rotation_to_grid and self.velocity.magnitude() < 0.005:
                self.velocity += (((self.location * 2).round_towards(self.velocity) / 2) - self.location) * 0.15 - self.velocity
            self.velocity /= 4
        shift_down = bool(pygame.key.get_mods() & pygame.KMOD_SHIFT)
        #kinda stupid way to limit top speed
        if self.velocity.magnitude() > (0.1 if not shift_down else 1):
            self.velocity = self.velocity.normalized() / 10
        self.location += self.velocity

        if self.angaccel:
            self.angvel += self.angaccel
            if abs(self.angvel) > 0.13: self.angvel = math.copysign(0.13, self.angvel)
        else:
            if self.snap_rotation_to_grid and self.angvel < 0.1:
                angle_increment = (math.tau / 8)
                f = math.ceil if self.angvel > 0 else math.floor
                nearest_dir = f(self.rotation / angle_increment) * angle_increment
                self.angvel += (nearest_dir - self.rotation) * 0.2 - self.angvel * 0.5
            self.angvel *= 0.6

        if map[math.floor(self.location + self.velocity)]:
            dx, dy = self.velocity
            px, py = self.location
            if map[math.floor(px + dx), math.floor(py)]:
                px -= dx * 1.2
                dx = -dx / 2
                #px = round(px)
            if map[math.floor(px), math.floor(py + dy)]:
                py -= dy * 1.2
                dy = -dy / 2
                #py = round(py)
            #self.accel = 0
            self.velocity = V(dx, dy) #
            self.location = V(px, py)


        self.angvel = max(-0.3, min(0.3, self.angvel))
        self.rotation += self.angvel
        self.rotation %= math.tau

    @controls.action("Forward", pygame.K_w)
    def forward_pressed(self, up):
        self.accel = 0 if up else 0.006
    @controls.action("Backward", pygame.K_s)
    def back_pressed(self, up):
        self.accel = 0 if up else -0.006
    @controls.action("Turn Left", pygame.K_a)
    def left_pressed(self, up):
        self.angaccel = 0 if up else -0.006
    @controls.action("Turn Right", pygame.K_d)
    def right_pressed(self, up):
        self.angaccel = 0 if up else 0.006

    def stop(self):
        self.accel = self.angaccel = 0


class GameState:
    def __init__(self):
        self.levelmusic = musicplayer.DoubleLayerMusicPlayer(Path("music", "lv1", "a.ogg"),
                                                             Path("music", "lv1", "b.ogg"))
        self.clock = pygame.time.Clock()
        self.camera = ControllableCamera()
        self.mainwin = gfx.MainWindow(self.camera)
        self.world_renderer = self.mainwin.world_view
        self.current_level = None #None represents town
        self.party = [] #type: list[character.PlayerCharacter]
        #self.party = [character.Warrior()] * 6
        #for the_character in self.party:
        #    the_character.inventory.append(character.PointyStick())
        self.battle = None
        self.paused = False
        self.partyfunds = 900
        controls.bindings.register(self)

    @property
    def current_level(self):
        return self._current_level
    @current_level.setter
    def current_level(self, newlevel):
        self.world_renderer.change_level(newlevel)
        self._current_level = newlevel
        self.camera.location = V(1.5, 1.5)
        if not newlevel:
            self.levelmusic.stop()
        else:
            self.levelmusic.play()

    def run_game(self) -> typing.NoReturn:
        # import character
        # testman = character.Warrior("Testman")
        # testman.inventory.append(character.Carbine())
        # gamestate.party.append(testman)
        # testman = character.Warrior("Testman 2")
        # testman.inventory.append(character.PointyStick())
        # gamestate.party.append(testman)
        # testman = character.Warrior("Testman 3")
        # testman.inventory.append(character.Carbine())
        # #testman.inventory.append(character.CombatFirstAidKit())
        # gamestate.party.append(testman)
        # self.start_battle()
        while True:
            self.frame()

    def frame(self):
        # controls.bindings.dispatch()
        # windowmanager.draw_windows(self.mainwin.window_surface)
        # self.battle.turn()
        # return
        try:
            self.mainwin.bottom_textbox.print_character_blocks(self.party)
            self.mainwin.world_view_active = self.current_level is not None
            self.mainwin.middle_textbox_active = self.battle is not None
            self.levelmusic.b_should_be_playing = (self.battle is not None)
            # self.mainwin.draw()

            # self.handle_keypresses()
            controls.bindings.dispatch()

            if self.battle:
                self.battle = self.battle.turn()
                self.party = self.battle.player_characters
                if self.battle.is_over:
                    if any(not c.incapacitated for c in self.party):
                        self.battle_victory(self.battle)
                    self.battle = None
            elif self.current_level is not None:
                self.camera.update(self.current_level.map)
                self.current_level.update(self)

                if math.isclose(time.perf_counter() % 15, 0):
                    for the_character in self.party:
                        the_character.process_status_effects()

                # if all(c.incapacitated for c in self.party):
                #    text_menu.pop_up_notification("Your party has fallen.")
                #    self.party.clear()
                #    self.current_level = None

            else:
                if not town.town_menu.window:
                    town.town_menu(self)

            self.mainwin.window_surface.fill(gfx.BGCOLOR)
            self.mainwin.bottom_textbox.print_character_blocks(self.party)
            self.mainwin.world_view_active = self.current_level is not None
            self.mainwin.middle_textbox_active = self.battle is not None
            self.mainwin.draw()
            windowmanager.draw_windows(self.mainwin.window_surface)

            #if random.random() > 0.9 and self.current_level:
            #    self.start_battle()

        except Exception:
            raise
            print(traceback.format_exc())
            print("You may or may not have to restart the game. Please tell me about this.")
        self.clock.tick(30)

    def start_battle(self, enemies = None):
        if not enemies:
            enemies = []
            for t in range(random.randint(3,5)):
                enemy = character.MurderousJerk(f"Murderous Jerk {t}", random.randint(5, 9), random.randint(5, 9), random.randint(5, 9), random.randint(5, 9), random.randint(5, 9), random.randint(5, 9))
                enemies.append(enemy)
                enemy.inventory.append(random.choice(character.MeleeWeapon.__subclasses__())())

        #text_menu.pop_up_notification("You are attacked by: " + "\n".join(str(e) for e in enemies))

        self.battle = battle.BattleState(self.party, enemies)
        battlewin = battle.BattleWindow(self.battle)

        #Camera won't get the release events when player releases key in battle
        self.camera.stop()

    def battle_victory(self, battle):
        text_menu.pop_up_notification("All enemies incapacitated or killed. You win!")


    def abandon_party(self):
        self.party.clear()
        self.current_level = None

    @controls.action("Esc Menu", pygame.K_ESCAPE)
    def esc_menu(self, up):
        if not up: return
        text_menu.PopUpMenu({
            "Quit" :         town.quit_confirmation,
            "Abandon party": text_menu.PopUpMenu({"Never mind": None,
                                                  "Leave all party members for dead": self.abandon_party},
                                                 prompt = "Are you absolutely sure? You will return to the town immediately, but your whole party will die."),
            "Back to game" : None
        })()

    @controls.action("Interact", ("MouseRight", pygame.K_e, pygame.K_TAB))
    def frob(self):
        if self.current_level and self.current_level.interaction_target:
            self.current_level.interaction_target.on_interact(self)

    @controls.action("CHEAT: Toggle Debug View", pygame.K_F7)
    def toggle_debug_view(self):
        self.mainwin.world_view.debug = not self.mainwin.world_view.debug

    @controls.action("CHEAT: Zoom Debug View Out", pygame.K_RIGHTBRACKET)
    def zoom_debug_view_out(self):
        self.mainwin.world_view.debug_view_scale += 2
    @controls.action("CHEAT: Zoom Debug View In", pygame.K_LEFTBRACKET)
    def zoom_debug_view_in(self):
        self.mainwin.world_view.debug_view_scale -= 2

    @controls.action("CHEAT: Warp to up ladder", pygame.K_F8)
    def warp_to_up_ladder(self):
        self.camera.location = self.current_level.end_loc + V(0.5, 0.5)


gamestate = GameState()